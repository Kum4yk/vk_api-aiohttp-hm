from aiohttp import web
from webargs.aiohttpparser import parser
from app.api.schemas import NameReqSchema


async def handle(request: web.Request):
    name = request.match_info.get('name', "Anonymous")
    text = "Hello, " + name
    try:
        request_data = await parser.parse(
            argmap=NameReqSchema,
            req=request,
            location='match_info'
        )
    except:
        pass

    return web.Response(text=text)


async def version(request: web.Request):
    """Возвращает номер версии сервиса"""
    config = request.app["config"]
    return web.Response(text=config["service"]["version"])

