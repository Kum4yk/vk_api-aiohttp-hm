import asyncpg


async def create_table(pool: asyncpg.pool.Pool):
    async with pool.acquire() as connection:
        await connection.execute(
            '''
            CREATE TABLE IF NOT EXISTS posts (
            group_link varchar(100),
            post_id int, 
            text varchar(15895),
            sentiment varchar(8)
            )
            '''
            )


async def insert_into_posts(pool: asyncpg.pool.Pool, values: tuple):
    group_link, post_id, text, sentiment = values
    async with pool.acquire() as connection:
        await connection.execute(
            f"""
            INSERT INTO posts(group_link, post_id, text, sentiment) 
            VALUES('{group_link}', {post_id}, '{text}', '{sentiment}');
            """
        )


if __name__ == "__main__":
    print("Hello ")
