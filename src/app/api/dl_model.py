import pathlib
from typing import Optional
import tensorflow as tf
import pandas as pd
import numpy as np
import pickle
from gensim.models import KeyedVectors
from app.api.handlers.data_handle import HandleData


class TfSentimentModel:
    mapper = {-1: "negative",
              0: "neutral",
              1: "positive"
              }
    OS_SEP = pathlib.os.sep

    def __init__(self, path_to_data: str):
        """
        :param path_to_data: path to data, ls:
        data
            models
                dl_model
                fasttext
            lemmas.pkl
        """
        self.__PATH_TO_DATA = path_to_data
        self.fasttext_model: Optional[KeyedVectors] = None
        self.tf_model: Optional[tf.keras.Model] = None
        self.data_handler: Optional[HandleData] = None

        self.set_fasttext_model(
            self.OS_SEP.join([self.__PATH_TO_DATA, "models", "fasttext", "model.model"])
        )
        self.set_data_handler(
            self.OS_SEP.join([self.__PATH_TO_DATA, "lemmas.pkl"])
        )

        self.set_tf_model(
            self.OS_SEP.join([self.__PATH_TO_DATA, "models", "dl_model"])
        )

    def set_fasttext_model(self, fasttext_path: str):
        """
        :param fasttext_path: path to fast_text_model
        """
        self.fasttext_model: KeyedVectors = KeyedVectors.load(fasttext_path)

    def set_tf_model(self, tf_model_path: str):
        """
        :param tf_model_path: path to saved tensorflow model
        """
        self.tf_model: tf.keras.Model = tf.keras.models.load_model(tf_model_path)

    def set_data_handler(self, lemmas_path: str = None):
        """
        :param lemmas_path: path to dictionary (from raw word to the lemma)
        """
        self.data_handler: HandleData = HandleData()
        if lemmas_path is not None:
            with open(lemmas_path, "rb") as lemmas_dict:
                self.data_handler.lemmas_dict = pickle.load(lemmas_dict)

    def predict_proba(self, text: str) -> np.ndarray:
        prepared_text = self.data_handler.transform_table(
            pd.Series(text),
            self.fasttext_model
        )
        predict_proba = self.tf_model.predict(prepared_text)
        return predict_proba

    def predict(self, text: str) -> str:
        predict_proba = self.predict_proba(text)[0]
        predict_class = np.argmax(predict_proba, axis=-1)
        predict_label = self.mapper[predict_class - 1]
        return predict_label


if __name__ == "__main__":
    PATH_TO_DATA = pathlib.os.sep.join(
        [str(pathlib.Path().absolute().parent.parent.parent),
         "data"]
    )
    print(PATH_TO_DATA)
    model = TfSentimentModel(PATH_TO_DATA)

    while True:
        message = input("print tested message or exit: ")
        if message == "exit":
            break
        print(model.predict(message))
