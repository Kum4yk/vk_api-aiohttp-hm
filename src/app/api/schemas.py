from marshmallow import Schema, fields


class NameReqSchema(Schema):
    name = fields.Str(required=True)


class VersionRespSchema(Schema):
    version = fields.Str(required=True)
