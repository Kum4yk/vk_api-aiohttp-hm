from aiohttp import web
import aiohttp_cors
from app.api.handlers.handlers import handle, version
from app.api.handlers.vk_handler import vk_handler


def setup_routes(app: web.Application):
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*"
        )
    })

    app.router.add_route("GET", "/", handle)
    app.router.add_route("GET", "/{name}", handle)
    app.router.add_route("GET", "/api/version", version)
    app.router.add_route("GET", "/{group_link}/{count}", vk_handler)

    for route in list(app.router.routes()):
        cors.add(route)

