from aiohttp import web
import asyncio
import asyncpg
import pathlib
from app.api.dl_model import TfSentimentModel
from app.api.routes import setup_routes
from app.setup_service.configuration import get_config
from app.setup_service.logs import init_logger
from app.setup_service.middlewares import log_middleware, error_middleware
from app.api.handlers.db_handler import create_table


async def init_app(model: TfSentimentModel, sql_params: dict, args: int = 0) -> web.Application:
    """Initialize the application server."""
    app = web.Application(
        middlewares=[log_middleware, error_middleware]
    )
    app["config"] = get_config(args)

    # create connection
    app["pool"]: asyncpg.pool.Pool = await asyncpg.create_pool(**sql_params)
    await create_table(app["pool"])

    app["tf_model"] = model
    setup_routes(app)
    init_logger(app["config"])
    return app


if __name__ == "__main__":
    params = {
        'database': 'vk-api_hm',
        'host': 'localhost',
        'user': 'postgres',
        'password': '123123'
    }

    PATH_TO_DATA = pathlib.os.sep.join(
        [str(pathlib.Path().absolute().parent.parent),
         "data"]
    )
    print(PATH_TO_DATA)
    final_model = TfSentimentModel(PATH_TO_DATA)

    print("\n\n\nGo here - http://localhost:8088/")

    # start(final_model, params)
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(init_app(final_model, params))
    web.run_app(
        app,
        port=app["config"]["service"]["port"],
        access_log=None
    )
