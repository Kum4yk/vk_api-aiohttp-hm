from http import HTTPStatus


class ServiceError(Exception):
    """Base class"""
    def __init__(
            self,
            message="Unexpected error",
            http_code=HTTPStatus.INTERNAL_SERVER_ERROR
    ):
        """

        :param message: error message
        :param http_code: code error
        """
        super().__init__(message)
        self.http_code = http_code
        self.message = message
