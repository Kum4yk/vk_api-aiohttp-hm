from aiohttp.web import middleware
from aiohttp import web, web_exceptions
from http import HTTPStatus
import time
import logging

@middleware
async def log_middleware(request: web.BaseRequest, handler):
    start_time = time.time()
    response: web.Response = await handler(request)
    info = {
        'endpoint': f"{request.method} {request.path}",
        "query": request.query_string,
        'request params': await request.text(),  # content body, но это не точно
        "time": f"{time.time() - start_time:.3}",
        "status_code": response.status,
        "response body": response.text
    }
    logging.info(info)
    return response


@middleware
async def error_middleware(request: web.BaseRequest, handler):
    """Обработка ошибок для каждого запроса."""
    try:
        return await handler(request)
    except web_exceptions.HTTPException as http_err:
        logging.exception(
            {"error": http_err.reason}
        )
        return web.json_response(
            {"error": http_err.reason},
            status=http_err.status
        )
    except Exception as error:
        logging.exception(
            {"error": error}
        )
        return web.json_response(
            {"error": error},
            status=HTTPStatus.INTERNAL_SERVER_ERROR
            # status=HTTPStatus.INTERNAL_SERVER_ERROR.value in lectures
        )
